$(function() {
  var pills = '<ul class="nav nav-pills">',
      $tabs = $('#content>p:first');
  /* style the info page */
  if ($tabs && $tabs.find('a[href*="action=info"]').length === 3) {
    $tabs.find('a[href*="action=info"]').each(function() {
      var pillRe = /"([^"]*)"/.exec($(this).text()),
          queryParts = $(this).attr('href').split('?'),
          active = ((queryParts.length === 2) &&
              (location.search === '?' + queryParts[1]));
      pills += '<li' + ((active) ? ' class="active"' : '') +
               '><a href="' + $(this).attr('href') +
               '">' + pillRe[1] + '</a></li>';
    });
    $tabs.replaceWith(pills + '</ul>');
  }
});

