# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - interim theme for wiki.list.org

    derived from 'modern' theme by Nir Soffer, Thomas Waldmann

    @copyright: 2015-2016 Jim Tittsler
    @license: GNU GPL, see COPYING for details.
"""

from MoinMoin.Page import Page
from MoinMoin.theme import ThemeBase

class Theme(ThemeBase):

    name = "listorg"

    stylesheets = (
        ('all', 'bootstrap.min'),
        ('all', 'common'),
        ('all', 'improvedtableparser'),
        ('all', 'includecomments'),
        ('all', 'screen'),
    )

    stylesheets_print = (
        ('all', 'bootstrap.min'),
        ('all', 'common'),
        ('all', 'improvedtableparser'),
        ('all', 'includecomments'),
        ('all', 'print'),
    )

    stylesheets_projection = (
        ('all', 'bootstrap.min'),
        ('all', 'common'),
        ('all', 'improvedtableparser'),
        ('all', 'includecomments'),
        ('all', 'projection'),
    )

    space_links = [
        ('COM/Home', 'Community'),
        ('DEV/Home', 'Development'),
        ('DOC/Home', 'Documentation'),
        ('SEC/Home', 'Security'),
        ]

    faq_links = [
        ('DOC/1 Introduction - What is GNU Mailman?', 'Intro to GNU Mailman'),
        ('DOC/2 Help for mailing list members', 'Mailing list members'),
        ('DOC/3 List administrator tasks', 'List administrator tasks'),
        ('DOC/4 Site administrator tasks', 'Site administrator tasks'),
        ('DOC/5 Downloading and installing Mailman', 'Downloading and installing'),
        ('DOC/6 Integration issues (with mail or web servers)', 'Integrating with mail and web servers'),
    ]

    def html_head(self, d):
        html = [
            ThemeBase.html_head(self, d),
            u'<link rel="shortcut icon" href="%s/%s/img/favicon.ico">' % (
                self.cfg.url_prefix_static, self.name),
            u'<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>',
            u'<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>',

        ]
        # add favicon
        return u'\n'.join(html)

    def logo(self):
        self.cfg.logo_string = '<img title="Mailman wiki" alt="Mailman logo" src="%s/%s/img/logo2010-2.jpg">' % (
            self.cfg.url_prefix_static, self.name)
        return ThemeBase.logo(self)

    def spaces(self):
        """ shortcuts to Spaces
        """
        return self.sidebar_list("spaces", self.space_links)

    def faq(self):
        """ shortcuts to FAQs
        """
        return self.sidebar_list("faqs", self.faq_links)

    def sidebar_list(self, list_id, links):
        """ shortcuts in a list for the given links
        """
        request = self.request
        fmt = request.formatter

        html = []
        append = html.append

        append(fmt.bullet_list(on=1, id=list_id))

        for pagename, label in links:
            page = Page(request, pagename)
            append(fmt.listitem(on=1))
            append(page.link_to(request, label))
            append(fmt.listitem(on=0))

        append(fmt.bullet_list(on=0))
        return u''.join(html)

    def sidebar(self, d):
        """ list.org-like sidebar
        """
        html = [
            u'<div class="col-md-3">',
            u'<div id="sidebar">',
            u'<div class="sidehead">Wiki</div>',
            self.navibar(d),
            u'<br>',
            self.editbar(d),
            u'<div class="sidehead">Spaces</div>',
            self.spaces(),
            u'<div class="sidehead">FAQs</div>',
            self.faq(),
            u'<div id="poweredby">',
            u'<a href="https://www.python.org/">',
            u'<img src="%s/%s/img/python-logo.png" alt="Python Powered logo" title="Python">' % (
                self.cfg.url_prefix_static, self.name),
            u'</a>',
            u'</div>',
            u'</div>',
            u'</div>',
        ]
        return u'\n'.join(html)

    def header(self, d, **kw):
        """ Assemble wiki header

        @param d: parameter dictionary
        @rtype: unicode
        @return: page header html
        """
        html = [
            # page wrapper
            u'<div id="mwrapper" class="container">',
            # Pre header custom html
            self.emit_custom_html(self.cfg.page_header1),

            # Header
            u'<nav id="navbar" class="navbar navbar-default">',
            u'<div class="container">',
            u'<div class="navbar-header">',

            u'<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">',
            u'<span class="sr-only">Toggle navigation</span>',
            u'<span class="icon-bar"></span>',
            u'<span class="icon-bar"></span>',
            u'<span class="icon-bar"></span>',
            u'</button>',
            u'<a class="navbar-brand" href="/"><img alt="Mailman logo" src="%s/%s/img/logo2010-2.jpg"></a>' % (self.cfg.url_prefix_static, self.name),
            #self.logo(),
            u'</div>',

            u'<div class="collapse navbar-collapse" id="navbar-collapse">',
            u'<div class="nav navbar-right">',
            self.username(d),
            u'<br>',
            self.searchform(d),
            u'</div>',
            u'</div>',

            #u'<div id="locationline">',
            #self.title(d),
            #u'</div>',
            #u'<div id="pageline"><hr style="display:none;"></div>',
            u'</div>',
            u'</nav>',

            # Post header custom html (not recommended)
            self.emit_custom_html(self.cfg.page_header2),

            u'<div class="container">',
            u'<div class="row">',

            # Sidebar
            self.sidebar(d),

            # right wrapper
            u'<div id="maincol" class="col-md-8">',
            self.msg(d),

            # Start of page
            self.startPage(),
        ]
        return u'\n'.join(html)

    def editorheader(self, d, **kw):
        """ Assemble wiki header for editor

        @param d: parameter dictionary
        @rtype: unicode
        @return: page header html
        """
        html = [
            # page wrapper
            u'<div id="mwrapper" class="container">',
            # Pre header custom html
            self.emit_custom_html(self.cfg.page_header1),

            # Header
            u'<nav id="navbar" class="navbar navbar-default">',
            u'<div class="container">',
            u'<div class="navbar-header">',

            u'<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">',
            u'<span class="sr-only">Toggle navigation</span>',
            u'<span class="icon-bar"></span>',
            u'<span class="icon-bar"></span>',
            u'<span class="icon-bar"></span>',
            u'</button>',
            u'<a class="navbar-brand" href="/"><img alt="Mailman logo" src="%s/%s/img/logo2010-2.jpg"></a>' % (self.cfg.url_prefix_static, self.name),
            #self.logo(),
            u'</div>',

            u'<div class="collapse navbar-collapse" id="navbar-collapse">',
            u'<div class="nav navbar-right">',
            self.username(d),
            u'<br>',
            self.searchform(d),
            u'</div>',
            u'</div>',

            u'</div>',
            u'</nav>',

            # Post header custom html (not recommended)
            self.emit_custom_html(self.cfg.page_header2),

            u'<div class="container">',
            u'<div class="row">',

            # Sidebar
            self.sidebar(d),

            # right wrapper
            u'<div id="maincol" class="col-md-8">',
            u'<div id="locationline">',
            self.title(d),
            u'</div>',
            self.msg(d),

            # Start of page
            self.startPage(),
        ]
        return u'\n'.join(html)

    def footer(self, d, **keywords):
        """ Assemble wiki footer

        @param d: parameter dictionary
        @keyword ...:...
        @rtype: unicode
        @return: page footer html
        """
        page = d['page']
        html = [
            # End of page
            self.pageinfo(page),
            self.endPage(),

            # row
            u'</div>',

            # Pre footer custom html (not recommended!)
            self.emit_custom_html(self.cfg.page_footer1),

            # Footer

            u'</div>',
            # maincol wrapper
            u'</div>',

            # content wrapper
            u'</div>',

            u'<nav id="footer" class="navbar navbar-default">',
            u'<div class="container">',
            u'''<p class="copyright navbar-text text-center">\u00A9 1998-2016
                Free Software Foundation, Inc.  Verbatim copying and distribution of this
                entire article is permitted in any medium, provided this notice is preserved.<br>
                Website built on the <a href="https://moinmo.in/">MoinMoin Wiki Engine</a>.</p>''',
            u'</div>',
            u'</nav>',

            # Post footer custom html
            self.emit_custom_html(self.cfg.page_footer2),

            u'<script src="%s/%s/js/listorg.js"></script>' % (
                self.cfg.url_prefix_static, self.name),
            ]
        return u'\n'.join(html)


def execute(request):
    """
    Generate and return a theme object

    @param request: the request object
    @rtype: MoinTheme
    @return: Theme object
    """
    return Theme(request)
